import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { observable, Observable } from 'rxjs';
import { EventEmitter } from 'events';

@Injectable({
  providedIn: 'root'
})



export class AppService {

  constructor(private http: HttpClient) { }

  rootURL = 'http://localhost:8080/api';
  project: any ;
  devisProjetId;

  getClient(){
    return this.http.get(this.rootURL+'/client');
  }
  
  getClientById(id: number){
    const url = `${this.rootURL+'/client'}/${id}`;
    return this.http.get(url);
  }

  getProjectByClientId(id: number){
    const url = `${this.rootURL+'/projet/client'}/${id}`;
    return this.http.get(url);
  }

  getCategorieByProjectId(id: number){
    const url = `${this.rootURL+'/categorie/project'}/${id}`;
    return this.http.get(url);
  }

  getDomainesByCategorieId(id: number){
    const url = `${this.rootURL+'/domaine/categorie'}/${id}`;
    return this.http.get(url);
  }

  getDomaineById(id: number){
    const url = `${this.rootURL+'/domaine'}/${id}`;
    return this.http.get(url);
  }

  getComplexiteByDomaine(id: number){
    const url = `${this.rootURL+'/complexite/domaine'}/${id}`;
    return this.http.get(url);
  }

  getProjetById(id: number){
    const url = `${this.rootURL+'/projet'}/${id}`;
    return  this.http.get(url);
  }

  getDeliveryById(id:number){
    const url = `${this.rootURL+'/delivery'}/${id}`;
    return  this.http.get(url);
  }


   createDevisProjet(nomProjet, commentaire, numContrat,  clientNom, clientId,
    adresse, identTva, refClient, telephone, siret){
    const url = `${this.rootURL+'/devisProjet'}`;

    return   this.http.post<any>(url, {
      nomProjet: nomProjet,
      commentaire: commentaire,
      numContrat: numContrat,
      clientId: clientId,
      clientNom: clientNom,
      adresse: adresse,
      identTva:identTva,
      refClient:refClient,
      telephone:telephone,
      siret:siret
    });
  }

  
  createDevis(p: number, nom: string, date: Date) {
    const url = `${this.rootURL+'/devis'}`;
    return  this.http.post<any>(url, {
      libelle: nom,
      //total montant
      //nb jour
      dateCreation: new Date(),
      dateValidation: date, //
      devisProjet:{
        devisProjetId: p
      }
    });
  }

  getAllComplexite(){
    const url = `${this.rootURL+'/complexite'}`;
    return this.http.get(url);
  }

  getDeliveryByProjectId(id: number){
    const url = `${this.rootURL+'/delivery/projet'}/${id}`;
    return this.http.get(url);
  }

  getRatioByDeliveryId(id: number){
    const url = `${this.rootURL+'/ratio/delivery'}/${id}`;
    return this.http.get(url);
  }

  getActiviteByActiviteId(id: number){
    const url = `${this.rootURL+'/activite'}/${id}`;
    return this.http.get(url);
  }

  getTjByDeliveryId(id: number){
    const url = `${this.rootURL+'/tj/delivery'}/${id}`;
    return this.http.get(url);
  }

  getProfilByProfilId(id: number){
    const url = `${this.rootURL+'/profil'}/${id}`;
    return this.http.get(url);
  }

  getEchancierByDeliveryId(id: number){
    const url = `${this.rootURL+'/echancier/delivery'}/${id}`;
    return this.http.get(url);
  }

  getEtapeByEtapeId(id: number){
    const url = `${this.rootURL+'/etapeEchancier'}/${id}`;
    return this.http.get(url);
  }

  createDevisItem(devisId, num, ev, fo){
    const url = `${this.rootURL+'/devisItem'}`;
    return  this.http.post<any>(url, {
      evolution: ev,
      fonction: fo,
      devis:{
          devisId: devisId
      } 
    });
    
    
  }

  createDevisComplexiteItem(devisItemId ){
    const url = `${this.rootURL+'/devisComplexiteItem'}`;
    return  this.http.post<any>(url, {
      devisItem: {
        devisItemId: devisItemId
      }
    });
  }

  createDevisComplexite(libelle,nom ,valeur,devisComplexiteItemId ,categorieDescription ,categorieId ,categorieName ,domaineId ,domaineDescription,domaineName){
    const url = `${this.rootURL+'/devisComplexite'}`;
    
    return  this.http.post<any>(url, {
      date : new Date(),
      libelle :libelle,
      nom :nom,
      valeur:valeur,
      categorieDescription :categorieDescription,
      categorieId :categorieId,
      categorieName :categorieName,

      domaineId :domaineId,
      domaineDesciption:domaineDescription,
      domaineName: domaineName,

      devisComplexiteItem :{
        devisComplexiteItemId: devisComplexiteItemId
      }

     
    });
  }

  createDevisRatioItem(devisItemId){
    const url = `${this.rootURL+'/devisRatioItem'}`;
    return  this.http.post<any>(url, {
      devisItem: {
        devisItemId: devisItemId
      }
    });
  }

  createDevisRatio(activiteId, activiteLibelle,activiteIdCourt,activiteIdLong,deliveryId, deliveryNom,deliveryLibelle, pourcentage,  ordreAffichage, devisRatioItemId){
    const url = `${this.rootURL+'/devisRatio'}`;
    return  this.http.post<any>(url, {
      date: new Date(),
      activiteId: activiteId,
      activiteLibelle: activiteLibelle,
      activiteIdCourt: activiteIdCourt,
      activiteIdLong: activiteIdLong,
      deliveryId: deliveryId, 
      deliveryNom: deliveryNom,
      deliveryLibelle:deliveryLibelle,
      pourcentage:pourcentage,  
      ordreAffichage:ordreAffichage, 
      devisRatioItem:{
        devisRatioItemId: devisRatioItemId
      }
    });
  }

  createDevisTjItem(devisItemId){
    const url = `${this.rootURL+'/devisTjItem'}`;
    return  this.http.post<any>(url, {
      devisItem: {
        devisItemId: devisItemId
      }
    });
  }
  
  createDevisTj(devisTjItemId,deliveryId2,deliveryLibelle2, deliveryNom2, profilId, profilLibelle, tjCout){
    const url = `${this.rootURL+'/devisTj'}`;
    return  this.http.post<any>(url, {
      date: new Date(),
      cout: tjCout,
      deliveryId: deliveryId2,
      libelle: deliveryLibelle2,
      nom: deliveryNom2,
      profilId: profilId,
      profilLibelle: profilLibelle,
      devisTjItem:{
        devisTjItemId: devisTjItemId
      }
    });
  }


  createDevisEcheancierItem(devisItemId){
    const url = `${this.rootURL+'/devisEchancierItem'}`;
    return  this.http.post<any>(url, {
      devisItem: {
        devisItemId: devisItemId
      }
    });
  }


  createDevisEcheancier(deliveryId,deliveryNom,deliveryLibelle,etapeEchancierId,etapeEchancierEtape,pourcentage2,devisEchancierItemId){
    const url = `${this.rootURL+'/devisEchancier'}`;
    return  this.http.post<any>(url, {
      date: new Date(),
      deliveryId : deliveryId,
      etapeEchancierId: etapeEchancierId,
      etapeEchancierEtape: etapeEchancierEtape,
      pourcentage: pourcentage2,
      deliveryNom: deliveryNom,
      deliveryLibelle: deliveryLibelle,
      devisEchancierItem:{
        devisEchancierItemId: devisEchancierItemId,
      }
    });
  }

  getAllDevis(){
    const url = `${this.rootURL+'/devis'}`;
    return this.http.get(url);
  }

  getByDevisId(id: number){
    const url = `${this.rootURL+'/devis'}/${id}`;
    return this.http.get(url);
  }

  putMontantDevis(montant){
    
  }

}
