import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageOneComponent } from './page-one/page-one.component';
import { PageDataComponent } from './page-data/page-data.component';
import { PageChiffrageComponent } from './page-chiffrage/page-chiffrage.component'; 
import { PageDeliveryComponent } from './page-delivery/page-delivery.component';
import { PageEchancierComponent } from './page-echancier/page-echancier.component';
import { PageSyntheseComponent } from './page-synthese/page-synthese.component';
import { PageVisualiserComponent } from './page-visualiser/page-visualiser.component';



const routes: Routes = [
  {path: 'appOne', component: PageOneComponent},
  {path: 'pageData', component: PageDataComponent},
  {path: '', component: PageOneComponent},
  {path: 'pageChiffrage', component: PageChiffrageComponent},
  {path: 'pageDelivery', component: PageDeliveryComponent},
  {path: 'pageEchancier', component: PageEchancierComponent},
  {path: 'pageSynthese', component: PageSyntheseComponent},
  {path: 'pageVisualiser', component: PageVisualiserComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
