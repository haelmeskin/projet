import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageOneComponent } from './page-one/page-one.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PageDataComponent } from './page-data/page-data.component';
import { MatFormFieldModule,MatInputModule, MatSelectModule, MatButtonModule, MatCardModule, MatGridListModule, MatTableModule} from '@angular/material';
import { PageChiffrageComponent } from './page-chiffrage/page-chiffrage.component'; 
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import { PageDeliveryComponent } from './page-delivery/page-delivery.component';
import { MatCheckboxModule } from '@angular/material';
import { PageEchancierComponent } from './page-echancier/page-echancier.component';
import { PageSyntheseComponent } from './page-synthese/page-synthese.component';
import { PageVisualiserComponent } from './page-visualiser/page-visualiser.component';


@NgModule({
  declarations: [
    AppComponent,
    PageOneComponent,
    PageDataComponent,
    PageChiffrageComponent,
    PageDeliveryComponent,
    PageEchancierComponent,
    PageSyntheseComponent,
    PageVisualiserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule, 
    MatCardModule,
    MatGridListModule,
    MatTableModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MatCheckboxModule
  ],
  providers: [],
  bootstrap: [AppComponent],
 
})
export class AppModule { }
