import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-page-synthese',
  templateUrl: './page-synthese.component.html',
  styleUrls: ['./page-synthese.component.css']
})
export class PageSyntheseComponent implements OnInit {

  constructor(private appService: AppService,private route: ActivatedRoute, private router: Router) { }

  client;
  projet;
  NomClient;
  NomProjet;
  model;
  total: number;
  etapes;
  Montant;
  dates;
  devis;
  devisId;
  delivery;
  evolution;function;ptu;charge;montant;
  montantTotal: number = 0
  dateCreation; dateValidation;
  nomdevis;


   ngOnInit() {
    this.route.queryParamMap.subscribe(params => {
      this.total = Number(params.get('total'));
      this.client = Number(params.get('client'));
      this.projet = params.get('projet');
      this.etapes = params.getAll('etapes');
      this.Montant = params.getAll('montant');
      this.dates = params.getAll('dates');
      this.model = params.get('delivery');
      this.evolution = params.getAll('evolution');
      this.function = params.getAll('function');
      this.ptu =  params.getAll('ptu');
      this.charge = params.getAll('charge');
      this.montant = params.getAll('montantTotal');
      this.devisId = Number(params.get('devisId'));
      
      for (let index = 0; index < this.montant.length; index++){
        this.montantTotal = this.montantTotal  + Number(this.montant[index]);
      }
      
    });

    this.appService.putMontantDevis(this.montantTotal);
    

    //get date
    this.appService.getByDevisId(this.devisId).subscribe(devis => {
      this.dateCreation = devis['dateCreation'];
      this.dateValidation = devis['dateValidation']
      this.nomdevis = devis['libelle']
    })


    //get client by id
    this.appService.getClientById(this.client).subscribe(Client => {
      this.NomClient =  Client['nom'];
    }); 

    //get Projet by id
    this.appService.getProjetById(this.projet).subscribe(Projet => {
      this.NomProjet =  Projet['nomProjet'];
    }); 

    
  }

  onSave(){
    this.router.navigate(['/pageVisualiser'])
  }

}
