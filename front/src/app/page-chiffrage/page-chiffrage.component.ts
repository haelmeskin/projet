import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AppService } from '../app.service';



@Component({
  selector: 'app-page-chiffrage',
  templateUrl: './page-chiffrage.component.html',
  styleUrls: ['./page-chiffrage.component.css']
})






export class PageChiffrageComponent implements OnInit {

  constructor(private appService: AppService,private route: ActivatedRoute, private router: Router) { }
  client : string
  projet : string
  projetId: number;
  categories : any[] = [] 
  domaines : any[] = []
  domaineNames: any[] = []
  complexite: any[] = []
  tableau: any[] = []
  count: any[] = []
  nom: string
  private fieldArray: Array<any> = [];
  private newAttribute: any = {};
  longeur: number = 0 ;
  row: Array<number>;
  AllComplexite: number[] = []
  AllComplexiteValue: number[] = []
  devisItemId: number;
  devisComplexiteItemId;

  //information of rows
  num: any[] = [];
  evolution : any[] = [];
  function: any[] = [];
  complexites: number[] = [];
  countRow: number = 0;
  countColumn: number = 0;
  total: number [] = [];
  index: number = -1;
  devisId;
//--------------------------------------
  libelle: any[] = [];
  nomP : any[] = [];
  valeur: any[] = [];
  devisComplexiteItemIdP ;
  categorieDescription: any[] = [];categorieId: any[] = []; categorieName: any[] = [];
  complexiteId ;complexiteLibelle;complexiteNom ;complexiteValeur;
  domaineId ;
  domaineDescription;
  domaineName;

//--------------------------------------

  ngOnInit() {
      this.route.queryParamMap.subscribe(params => {
        this.client = params.get('nomClient');
        this.projet = params.get('nomProjet');
        this.categories = params.getAll('listCategorie');
        this.domaines = params.getAll('listDomaine');
        this.projetId = Number(params.get('projetId'));
        this.devisId = Number(params.get('devisId'));

        console.log(this.domaines)

        //getAllComplexite
        this.appService.getAllComplexite().subscribe((AllComplexite: any[]) => {
          this.AllComplexite =  this.AllComplexite.concat(AllComplexite);
        });  
       
        //get domaine by name
        for (let index = 0; index < this.domaines.length; index++) {
          this.appService.getDomaineById(this.domaines[index]).subscribe((domaineNames: any[]) => {
            this.domaineNames =  this.domaineNames.concat(domaineNames['nom']);

          });          
        }

        //get complexite by domaine
        for (let index = 0; index < this.domaines.length; index++) {
          this.appService.getComplexiteByDomaine(this.domaines[index]).subscribe((complexite: any[]) => {
            this.count[index] = complexite.length;  
            this.complexite = complexite;


           
            this.tableau =  this.tableau.concat(complexite)

           
          });
               
        }
      });
  }


  //ajouter une ligne
  addItem(){
    
    this.index++;

    
    this.longeur = this.longeur + 1
    this.row = new Array(this.longeur)
    for (let index = 0; index < this.row.length; index++) {
      this.row[index] = index + 1;
    }

    //nombre de ligne
    this.countRow++;
    
    for (let index = 0; index < this.AllComplexite.length; index++) {
      //mettre le valeurs saisit pour chaque complexités dans une tables
      //initialiser la table par des zéro
      this.complexites[index] = 0;
      //mettre les complexites dans une tables
      //initialiser la table par des zéro
      this.AllComplexiteValue[index] = 0
    }
    //add complexites to tables
  }

  

  //every time, I enter a value I call this function onKeyXXX
  //à chaque fois, j'entre une valeur J'appelle cette fonction onKeyXXX

  //Pour le numero de la demande
  onKeyNum(event: any) { // without type info    
    this.num[this.countRow] = event.target.value

  }

  //Pour le champ évolution
  onKeyE(event: any) { // without type info
    this.evolution[this.countRow] = event.target.value ;
  }

  //Pour le champ fonction
  onKeyF(event: any) { // without type info
    this.function[this.countRow] = event.target.value ;
  }

  //Pour les champs complexités
  onKeyC(event: any, column, id) { // without type info

    //si je supprime une valeur (value == empty) 
    if(event.target.value=="")
      this.complexites[id] = Number(0);
    else{
    //si j'ajoute une valeur (value == 40) 
      this.complexites[id] = event.target.value ;
    }
    
    this.AllComplexiteValue[id] = column
    
    //Le calcul du total
    this.total[this.index] = 0;
    
    for (let index = 0; index < this.AllComplexite.length; index++) {
      this.total[this.index] = this.total[this.index] + (this.complexites[index] *  this.AllComplexiteValue[index])
    }
  }

  //chaque ligne est un item devis (evolution, fonction)

  //devis complexite contient les complexites
  //complexite_tem = liste des devis complexite

  //on Submit
  onSave(){

    
//code répéter

 //create devis item + num devis
    for(let index = 1; index <  this.evolution.length ; index ++){
      this.appService.createDevisItem(this.devisId, this.num[index], this.evolution[index], this.function[index]).subscribe((data: any[]) => {
        this.devisItemId  = data['devisItemId'];
        

        //create devis_complexite_item
        this.appService.createDevisComplexiteItem(this.devisItemId).subscribe(data => {
          this.devisComplexiteItemId  = data['devisComplexiteItemId'];

          //get domaine by name
          for (let index = 0; index < this.domaines.length; index++) {
            this.appService.getDomaineById(this.domaines[index]).subscribe((domaineName: any[]) => {
              this.domaineName =  domaineName['nom'];
              this.domaineId  =  (domaineName['domaineId']);
              this.domaineDescription  =  (domaineName['libelle']);
              
             
              
              this.appService.getComplexiteByDomaine(this.domaines[index]).subscribe(complexite => {
                //get complexite information  
                for (let index = 0; index < Object.entries(complexite).length; index++) {
                  this.libelle = this.libelle.concat((complexite[index])['libelle'])
                  this.nomP = this.nomP.concat((complexite[index])['nom'])
                  this.valeur = this.valeur.concat((complexite[index])['valeur'])
                  this.categorieDescription = ((((complexite[index])['domaine'])['categorie'])['libelle'])
                  this.categorieId = ((((complexite[index])['domaine'])['categorie'])['categorieId'])
                  this.categorieName = ((((complexite[index])['domaine'])['categorie'])['nom'])

               
                  //create devis_complexite
                  this.appService.createDevisComplexite(
                    this.libelle[index], this.nomP[index], this.valeur[index],
                    this.devisComplexiteItemId,
                    this.categorieDescription, this.categorieId, this.categorieName,
                    this.domaineId, this.domaineDescription, this.domaineName).subscribe(data => {
                      console.log()
                    });
                }
              });
            });
          }
        });  
      });
    }
    console.log(this.devisItemId)
    this.router.navigate(['/pageDelivery'],
    {queryParams: 
      {
        num: this.num,
        evolution: this.evolution, 
        function: this.function,
        complexites: this.complexites,
        projetId: this.projetId,
        totals: this.total,
        client: this.client,
        projet: this.projet,
        devisId: this.devisId
        }
      }).then(()=>{
        window.location.reload();
      });
    }
   

   
    
   

}
