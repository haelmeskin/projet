import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-page-visualiser',
  templateUrl: './page-visualiser.component.html',
  styleUrls: ['./page-visualiser.component.css']
})
export class PageVisualiserComponent implements OnInit {

  constructor(private appService: AppService,private route: ActivatedRoute, private router: Router) { }

  devisId: any[] = [];
  client: any[] = [];
  categorie: any[] = [];
  projet: any[] = [];
  date:any[] = [];
  contrat: any[]=[];
  adresse: any[]=[];
  identTva: any[]=[];
  refClient: any[]=[];
  telephone: any[]=[];
  siret: any[]=[];
  dateValidation: Date[]=[];
  dateCreation: Date[]=[];
  devisNom: any[]=[];
  nbJours: any[] = [];

  

  ngOnInit() {
    
    //get devis
    this.appService.getAllDevis().subscribe(devis => {
      for(let i =0;i<Object.entries(devis).length; i++){
          this.devisId = this.devisId.concat((devis[i])['devisId'])
          this.dateCreation = this.dateCreation.concat((devis[i])['dateCreation'])
          this.dateValidation = this.dateValidation.concat((devis[i])['dateValidation'])

          this.devisNom = this.devisNom.concat((devis[i])['libelle'])

          this.client = this.client.concat(((devis[i])['devisProjet'])['clientNom'])
          this.categorie = this.categorie.concat(((devis[i])['devisProjet'])['categorieNom'])
          this.projet = this.projet.concat(((devis[i])['devisProjet'])['nomProjet'])
          this.contrat = this.contrat.concat(((devis[i])['devisProjet'])['numContrat'])
         
          this.adresse = this.adresse.concat(((devis[i])['devisProjet'])['adresse'])
          this.identTva = this.identTva.concat(((devis[i])['devisProjet'])['identTva'])
          this.refClient = this.refClient.concat(((devis[i])['devisProjet'])['refClient'])
          this.telephone = this.telephone.concat(((devis[i])['devisProjet'])['telephone'])
          this.siret = this.siret.concat(((devis[i])['devisProjet'])['siret'])
          this.date = this.date.concat((devis[i])['dateValidation'])
         
      }
      

      for(let i=0;i<this.dateCreation.length; i++){
        var time_diff = new Date(this.dateValidation[i]).getTime() - new Date(this.dateCreation[i]).getTime()
        var days_Diff : number = time_diff / (1000 * 3600 * 24);
        
        this.nbJours = this.nbJours.concat(Math.round(days_Diff));
      }
      
      
    }); 
  }

  onRetour(){
    this.router.navigate(['/appOne'])
  }

}
