import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import {FormControl, FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import {MatFormFieldModule, MatInputModule} from '@angular/material';





@Component({
  selector: 'app-page-data',
  templateUrl: './page-data.component.html',
  styleUrls: ['./page-data.component.css'],

})

export class PageDataComponent implements OnInit {
  constructor(private appService: AppService, private formBuilder: FormBuilder,  private router: Router) { }

  users: any[] = [];
  projects: any[] = [];
  categories: any[]= [];
  domaines: any[]= [];
  isClose: boolean;
  valueSelected: string;
  selectedCat = new FormControl();
  selectedDom = new FormControl();
  myGroup: FormGroup;
  cat = [];
  dom = [];
  date;
  nomDevis;
  projetId: number
  clientId: number
  devisProjetId;
  nomProjet;
  commentaire;
  numContrat;
  devisId: number;
 clientId2;clientNom;adresse;identTva;refClient;telephone;siret;


  ngOnInit() {
    this.myGroup = this.formBuilder.group({
      nomDevis: ['', Validators.required],
      date: ['', Validators.required],
      nom: ['', Validators.required],
      projet: ['', Validators.required],
      dom: [[], Validators.required],
      cat: [[], Validators.required]
      
    });

    this.appService.getClient().subscribe((users: any[]) => {
      this.users = users;
    });
    
  }

  //récupérer le nom du client
  changeClient(selectedClient) {
    this.clientId = selectedClient
    this.appService.getProjectByClientId(selectedClient).subscribe((projects: any[]) => {
      this.projects = projects;
      console.log(projects)
    });
  }

  //récupérer le nom du projet selon le client choisit
  async changeProject(selectedProject){
    this.projetId = selectedProject;
    await this.appService.getCategorieByProjectId(selectedProject).subscribe((categories: any[]) => {
      this.categories = categories;
      this.appService.getProjetById(this.projetId).subscribe(project => {
        this.nomProjet= project['nomProjet'],
        this.commentaire= project['commentaire'],
        this.numContrat= project['numContrat']
        this.clientId2 = (project['client'])['clientId']
        this.clientNom = (project['client'])['nom']
        this.adresse = (project['client'])['adresse']
        this.identTva = (project['client'])['identTva']
        this.refClient = (project['client'])['refClient']
        this.telephone = (project['client'])['telephone']
        this.siret = (project['client'])['siret']



         //create devis project
        this.appService.createDevisProjet(this.nomProjet, this.commentaire, this.numContrat, this.clientNom, this.clientId, 
          this.adresse, this.identTva, this.refClient, this.telephone, this.siret).subscribe(data => {
          this.devisProjetId = data['devisProjetId'];
        });
      });
  
       

    });
  }

  //récupérer les categories selon le projet choisit
  changeCategorie(selectedCategories){
    this.isClose = false;
    if(!selectedCategories) {
      this.isClose = true;
      this.domaines = []
      this.myGroup.value.cat = this.selectedCat.value
      for(let i=0; i<this.selectedCat.value.length;i++){
        this.appService.getDomainesByCategorieId(this.selectedCat.value[i]).subscribe((domaines: any[]) => {
          this.domaines = this.domaines.concat(domaines);
        });
      }
    }
  }

  //récupérer les domaines selon les catégories choisit
  changeDomaine(selectedDomaines){
    this.isClose = false;
    if(!selectedDomaines) {
      this.isClose = true;
      this.myGroup.value.dom = this.selectedDom.value

    }
  }

onSubmit() {


        //create devis
        (this.appService.createDevis(this.devisProjetId, this.myGroup.value.nomDevis, this.myGroup.value.date)).subscribe(data => {
          this.devisId = data['devisId']

          console.log()
          this.router.navigate(['/pageChiffrage'],
            {queryParams: 
              {
                nomClient: this.myGroup.value.nom, 
                nomProjet: this.myGroup.value.projet,
                listCategorie: this.myGroup.value.cat,
                listDomaine: this.myGroup.value.dom,
                projetId: this.projetId,
                devisId: data['devisId']
              }, 
            
            }
        );
      })
  

      
    

    

    
 
  }
}
