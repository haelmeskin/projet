import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AppService } from '../app.service';


@Component({
  selector: 'app-page-echancier',
  templateUrl: './page-echancier.component.html',
  styleUrls: ['./page-echancier.component.css']
})
export class PageEchancierComponent implements OnInit {

  constructor(private appService: AppService,private route: ActivatedRoute, private router: Router) { }

  client;
  projet;
  model;
  modelDelivery;
  total: number;
  projetId: number
  delivery: any[] = []
  etapes: any[] = []
  NomEtapes: any[] = []
  pourcentage: number[] = []
  indexP: number = 0
  totalP: number = 0;
  Montant: number[] = []
  indexMontant: number = 0;
  tableDates: Date[] = [];
  devisId;
  evolution;function;ptu;charge;montant
  //---------------------------
  deliveryId; deliveryNom;deliveryLibelle;
  etapeEchancierId;etapeEchancierEtape;
  pourcentage2;devisEchancierItemId;


  ngOnInit() {
    this.route.queryParamMap.subscribe(params => {
      this.total = Number(params.get('total'));
      this.projetId = Number(params.getAll('projetId'));
      this.client = params.get('client');
      this.projet = params.get('projet');
      this.model = params.get('model');
      this.devisId = Number(params.get('devisId'));
      this.evolution = params.getAll('evolution');
      this.function = params.getAll('function');
      this.ptu =  params.getAll('ptu');
      this.charge = params.getAll('charge');
      this.montant = params.getAll('montant');
    });
      //get modele de delivery par projet
      this.appService.getDeliveryByProjectId(this.projetId).subscribe((delivery: any[]) => {
        this.delivery = delivery;
      });
  }
 
//appeler lors du choix du modele de delivery
  changeModele(selectedModel){

    //get delivery by id
    this.appService.getDeliveryById(selectedModel).subscribe(d =>{
      this.modelDelivery = d['nom'];
    })

    //get etapes d'echéancier par delivery id
    this.appService.getEchancierByDeliveryId(selectedModel).subscribe((tjs: any[]) => {
      this.totalP = 0
      
      for (let index = 0; index < Object.entries(tjs).length; index++) {

        
        //récupérer le pourcentage de chaque étape
        this.pourcentage[this.indexP] = (Object.entries(tjs)[index])[1].pourcentage

        //calcul du total du pourcentage (100%)
        this.totalP = this.totalP + this.pourcentage[this.indexP];

        //calcul du montant selon le pourcentage
        this.Montant[this.indexP] = this.total * (this.pourcentage[this.indexP]/100);
        this.indexP++;

        this.appService.getEtapeByEtapeId((Object.entries(tjs)[index])[1].echancierId.etapeEchancierId).subscribe((etapes: any[]) => {
          //Devis item echéancier
          this.appService.createDevisEcheancierItem(this.devisId).subscribe(data => {
            this.devisEchancierItemId  = data['devisEchancierItemId'];
            this.etapes = this.etapes.concat(etapes);
            this.NomEtapes = this.NomEtapes.concat(etapes['etape'])
            console.log(etapes)
            this.deliveryId = ((tjs[index])['delivery'])['deliveryId']
            this.deliveryNom = ((tjs[index])['delivery'])['nom']
            this.deliveryLibelle= ((tjs[index])['delivery'])['libelle']

            this.etapeEchancierId = etapes['etapeEchancierId']
            this.etapeEchancierEtape = etapes['etape']

            this.pourcentage2 = (tjs[index])['pourcentage']
     
          
            //Devis echéancier
            this.appService.createDevisEcheancier(this.deliveryId,this.deliveryNom,this.deliveryLibelle,
              this.etapeEchancierId,this.etapeEchancierEtape,this.pourcentage2,this.devisEchancierItemId).subscribe(data => {
                  console.log(data)
            });
          });
        });
      }
    });
  }

//appeler lors de la saisi du pourcentage 
  onKeyPourcentage(event, i :number){
    this.totalP  = 0
    for(let index =0; index < this.etapes.length; index++){
      if(index == i)
        this.pourcentage[index] = Number(event.target.value);
    
      this.totalP = this.totalP + this.pourcentage[index];
    }

    //calcul Montant
    for(let index =0; index < this.etapes.length; index++){
      this.Montant[index] = this.total * (this.pourcentage[index]/100);
    }
  }

  onKeyDate(event, i: number){
    for(let index =0; index < this.etapes.length; index++){
      if(index == i){
        this.tableDates[index] = event.target.value;
      }
    }
  }



  onSave(){
    this.router.navigate(['/pageSynthese'],
    {queryParams: 
      {
        client: this.client,
        projet: this.projet,
        total: this.total,
        model: this.model,
        etapes: this.NomEtapes,
        montant: this.Montant,
        dates: this.tableDates,
        delivery: this.modelDelivery,
        devisId: this.devisId,
        evolution: this.evolution ,
        function: this.function,
        ptu: this.ptu ,
        charge: this.charge ,
        montantTotal: this.montant,
      }
      });
  }

}
