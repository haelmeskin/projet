import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageDeliveryComponent } from './page-delivery.component';

describe('PageDeliveryComponent', () => {
  let component: PageDeliveryComponent;
  let fixture: ComponentFixture<PageDeliveryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageDeliveryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageDeliveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
